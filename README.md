# Comandos GIT utilizados

- git init --initial-branch=main
- git remote add origin git@gitlab.com:hdhumanez/frontend.git
- git add .
- git commit -m "Initial commit"
- git push --set-upstream origin main

# Pasos para ejecutar la REST API

- Debe estar corriendo ambos proyectos para su correcto funcionamiento.
- Ubicarse en la raiz del proyecto
- Ejecutar docker-compose up
- En el navegador ingresar a http://localhost:8080
- Ingresar a la base de datos con las siguientes credenciales
- Sistema: PostgreSQL
- Servidor: db
- Usuario: postgres
- Contraseña: 300x39z40l
- Base de datos: payroll
- Importar el archivo config.sql en Adminer
- Ejecutar el archivo config.sql
- Ejecutar pnpm i
- Ejecutar pnpm dev

## Config. backend

Para ejecutar en desarrollo debe tener instalado pnpm.

Para desarrollo

```bash
  pnpm install
  pnpm run dev
```

Tambien tienes otros scripts adicionales para hacer debugging y hacer pre-commit con husky para impedir hacer push a ramas si tienen errores de linter con eslint. lo que obliga a desarrolladores a seguir las reglas de eslint.

```bash
  pnpm run debug
  pnpm run prepare
```

Para producción

```bash
  pnpm install
  pnpm run build
  pnpm start
```

## Config. Frontend

Para ejecutar en desarrollo debe tener instalado node.

- Clonar el repositorio del frontend https://gitlab.com/hdhumanez/frontend

Para desarrollo

```bash
  pnpm i
  pnpm run dev
```

Para producción

```bash
  pnpm i
  pnpm run build
  pnpm run preview
```

## Authors

- [@hdhumanez](https://www.gitlab.com/hdhumanez)
