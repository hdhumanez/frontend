import { TRequestSchema } from "../schemas/request.schema";

const API_URL = import.meta.env.VITE_API_URL;

const addRequest = async (request: TRequestSchema, token: string) => {
  const response = await fetch(`${API_URL}/requests`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(request),
  });
  return response.json();
};

const getRequests = async (token: string) => {
  const response = await fetch(`${API_URL}/requests`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.json();
};

const updateRequest = async (
  id: string,
  request: TRequestSchema,
  token: string
) => {
  const response = await fetch(`${API_URL}/requests/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(request),
  });
  return response.json();
};

const deleteRequest = async (id: number, token: string) => {
  const response = await fetch(`${API_URL}/requests/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.json();
};

export { addRequest, getRequests, updateRequest, deleteRequest };
