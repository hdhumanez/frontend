import { TEmployeeSchema } from "../schemas/employee.schema";

const API_URL = import.meta.env.VITE_API_URL;

const addEmployee = async (employee: TEmployeeSchema, token: string) => {
  const response = await fetch(`${API_URL}/employees`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(employee),
  });
  return response.json();
};

const getEmployees = async (token: string) => {
  const response = await fetch(`${API_URL}/employees`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.json();
};

const updateEmployee = async (
  id: string,
  employee: TEmployeeSchema,
  token: string
) => {
  const response = await fetch(`${API_URL}/employees/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(employee),
  });
  return response.json();
};

const deleteEmployee = async (id: number, token: string) => {
  const response = await fetch(`${API_URL}/employees/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.json();
};

export { addEmployee, getEmployees, updateEmployee, deleteEmployee };
