import { IUserContext } from "../interfaces/user.interface";

export const falseUser: IUserContext["auth"] = {
  admin: {
    email: "hdhumanez@gmail.com",
    id: 1,
    name: "Heyner Humanez",
  },
  token:
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMWIwMTBhOTk3MjNlYjI2YmY0YWRmNyIsImVtYWlsIjoiaGRodW1hbmV6QGdtYWlsLmNvbSIsImlhdCI6MTY0NjE0Nzk4MywiZXhwIjoxNjQ2MTUxNTgzfQ.7DoXAIwPBLgHLYd3QCHHHXaBgEzRZgv1D09pbv-q3y8",
};
