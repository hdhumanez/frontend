import Home from "./pages/Home";
import NotFound from "./pages/_404";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import UserContext from "./context/UserContext";
import Welcome from "./pages/Dashboard/Welcome";
import AddEmployee from "./pages/Dashboard/AddEmployee";
import AddRequest from "./pages/Dashboard/AddRequest";
import Employees from "./pages/Dashboard/Employees";
import Requests from "./pages/Dashboard/Requests";
import EditEmployee from "./pages/Dashboard/EditEmployee";
import EditRequest from "./pages/Dashboard/EditRequest";

import { Routes, Route, BrowserRouter } from "react-router-dom";
import { PrivateRoute } from "./context/PrivateRoute";
import { useEffect, useMemo, useState } from "react";
import { IsLogged } from "./context/IsLogged";
import { IUserContext } from "./interfaces/user.interface";

function App() {
  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem("auth");
    if (loggedUserJSON) {
      const auth = JSON.parse(loggedUserJSON);
      setAuth(auth);
    }
  }, []);

  const [auth, setAuth] = useState<IUserContext["auth"]>();
  const value = useMemo(() => ({ auth, setAuth }), [auth, setAuth]);

  return (
    <main>
      <BrowserRouter>
        <UserContext.Provider value={value as IUserContext}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<IsLogged />}>
              <Route path="" element={<Login />} />
            </Route>
            <Route path="/signup" element={<IsLogged />}>
              <Route path="" element={<Signup />} />
            </Route>

            <Route path="/dashboard" element={<PrivateRoute />}>
              <Route
                path=""
                element={
                  <Dashboard>
                    <Welcome />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/employees"
                element={
                  <Dashboard>
                    <Employees />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/requests"
                element={
                  <Dashboard>
                    <Requests />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/employees/add"
                element={
                  <Dashboard>
                    <AddEmployee />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/employees/:id/edit"
                element={
                  <Dashboard>
                    <EditEmployee />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/requests/add"
                element={
                  <Dashboard>
                    <AddRequest />
                  </Dashboard>
                }
              />
              <Route
                path="/dashboard/requests/:id/edit"
                element={
                  <Dashboard>
                    <EditRequest />
                  </Dashboard>
                }
              />
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </UserContext.Provider>
      </BrowserRouter>
    </main>
  );
}

export default App;
