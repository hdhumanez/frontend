const EMPLOYEES_HEADER_TABLE = [
  "Nombre completo",
  "Fecha de admisión",
  "Salario",
  "Opciones",
];

const REQUESTS_HEADER_TABLE = [
  "Código",
  "Descripción",
  "Resumen",
  "Empleado",
  "Opciones",
];

const POKEMONS_HEADER_TABLE = ["Nombre", "Opción"];

export { EMPLOYEES_HEADER_TABLE, POKEMONS_HEADER_TABLE, REQUESTS_HEADER_TABLE };
