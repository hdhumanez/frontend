import { IUserContext } from "../../interfaces/user.interface";

export const USER_INITIAL_STATE: IUserContext["auth"] = {
  admin: {
    email: "",
    id: -1,
    name: "",
  },
  token: "",
};
