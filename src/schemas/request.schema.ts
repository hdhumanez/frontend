import { z } from "zod";

// Schemas
export const requestSchema = z.object({
  description: z
    .string()
    .min(2, { message: "La descripción debe tener al menos 2 caracteres" }),
  resume: z
    .string()
    .min(2, { message: "El resumen debe tener al menos 2 caracteres" }),
  employee_id: z
    .number()
    .positive({ message: "Ingrese un id de empleado válido" }),
});

// Types
export type TRequestSchema = z.infer<typeof requestSchema>;
