import { z } from "zod";

// Schemas
const adminSchema = z.object({
  email: z.string().email({ message: "Ingrese un correo válido" }),
  password: z
    .string()
    .min(6, { message: "La contraseña debe tener al menos 6 caracteres" }),
  name: z
    .string()
    .min(2, { message: "El nombre completo debe tener al menos 2 caracteres" })
    .max(50, {
      message: "El nombre completo no debe exceder los 50 caracteres",
    }),
});

const loginSchema = z.object({
  email: z.string().email({ message: "Credenciales inválidas" }),
  password: z.string({ message: "Credenciales inválidas" }),
});

// Types
type TAdminSchema = z.infer<typeof adminSchema>;
type TLoginSchema = z.infer<typeof loginSchema>;

export type { TAdminSchema, TLoginSchema };
export { adminSchema, loginSchema };
