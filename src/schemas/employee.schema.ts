import { z } from "zod";

// Schemas
export const employeeSchema = z.object({
  id: z.number().positive().optional(),
  admission_date: z.string().date(),
  name: z
    .string()
    .min(2, { message: "El nombre completo debe tener al menos 2 caracteres" })
    .max(50, {
      message: "El nombre completo no debe exceder los 50 caracteres",
    }),
  salary: z.number().positive({ message: "Ingrese un salario válido" }),
});

// Types
export type TEmployeeSchema = z.infer<typeof employeeSchema>;
