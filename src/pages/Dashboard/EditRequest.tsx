import { updateRequest } from "../../services/request.service";
import useAuth from "../../context/useAuth";
import toast, { Toaster } from "react-hot-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { TRequestSchema, requestSchema } from "../../schemas/request.schema";
import { useForm } from "react-hook-form";
import { IEmployee } from "../../interfaces/employee.interface";
import { useEffect, useState } from "react";
import { getEmployees } from "../../services/employee.service";
import { useLocation, useParams } from "react-router-dom";
import { IRequest } from "../../interfaces/request.interface";

const EditRequest: React.FC = () => {
  const { auth } = useAuth();
  const { state } = useLocation();
  const { id } = useParams<{ id: string }>();
  const { request_description, employee_id, request_resume } =
    state as IRequest;
  console.log(state, id);
  const [employees, setEmployees] = useState<IEmployee[]>([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TRequestSchema>({
    resolver: zodResolver(requestSchema),
    defaultValues: {
      description: request_description,
      employee_id,
      resume: request_resume,
    },
  });

  useEffect(() => {
    getEmployees(auth.token).then((response) => {
      if (response.success) {
        console.log(response.data);
        setEmployees(response.data);
      }
    });
  }, [auth.token]);

  const onSubmit = (data: TRequestSchema) => {
    if (!id) return toast.error("No se ha encontrado el id de la solicitud");
    updateRequest(id, data, auth.token).then((response) => {
      if (response.success) {
        toast.success("Solicitud actualizada correctamente");
      } else {
        return toast.error(response.info);
      }
    });
  };

  return (
    <section className="flex items-center justify-center md:p-24">
      <Toaster />
      <form
        className="flex flex-col items-center gap-2"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="description">
            Descripción
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("description")}
          />
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="resume">
            Resumen
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("resume")}
          />
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-300" htmlFor="employee_id">
            Empleados
          </label>

          <select
            className="w-full rounded-lg border border-gray-300 py-2 px-3 text-sm"
            {...register("employee_id", { valueAsNumber: true })}
          >
            {employees.length > 0 &&
              employees.map((emp) => (
                <option value={emp.id} key={emp.id}>
                  {emp.name}
                </option>
              ))}
          </select>
          <p className="text-xs text-red-200">{errors.employee_id?.message}</p>
        </div>
        <button
          type="submit"
          className="w-full sm:w-1/2 mt-6 rounded-lg text-white bg-cyan-600 hover:bg-cyan-700 py-2 font-medium shadow-lg transition-colors"
        >
          Actualizar solicitud
        </button>
      </form>
    </section>
  );
};

export default EditRequest;
