import { useEffect, useState } from "react";
import { IEmployee } from "../../interfaces/employee.interface";
import { getEmployees, deleteEmployee } from "../../services/employee.service";
import useAuth from "../../context/useAuth";
import toast, { Toaster } from "react-hot-toast";
import { EMPLOYEES_HEADER_TABLE } from "../../utils/constants/table";
import Table from "../../components/Table";
import { Link } from "react-router-dom";

const Employees = () => {
  const { auth } = useAuth();
  const { token } = auth;
  const [employees, setEmployees] = useState<Array<IEmployee>>([]);

  const doDeleteEmployee = (id: number) => {
    deleteEmployee(id, token).then((data) => {
      if (!data.success) {
        return toast.error(data.info);
      }
      const filteredPokemons = employees.filter((emp) => emp.id !== id);
      setEmployees(filteredPokemons);
    });
  };

  useEffect(() => {
    getEmployees(token).then((data) => setEmployees(data.data));
  }, [token]);

  return (
    <>
      <Toaster />
      <Table
        title="Mis empleados"
        description="Esta es tu lista de empleados registrados"
        header={EMPLOYEES_HEADER_TABLE}
      >
        {employees &&
          employees.map((employee, index) => (
            <tr
              key={employee.name}
              className={`${index % 2 == 1 ? "bg-gray-50" : null}`}
            >
              <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-900">
                {employee.name}
              </td>
              <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-500">
                {new Date(employee.admission_date).toLocaleDateString()}
              </td>
              <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-900">
                {employee.salary}
              </td>
              <td className="p-4 whitespace-nowrap text-gray-600">
                <Link
                  to={`/dashboard/employees/${employee.id}/edit`}
                  state={{
                    ...employee,
                    admission_date: employee.admission_date.split("T")[0],
                  }}
                  className="bg-yellow-600 mr-3 text-white font-bold rounded-md px-2 py-1"
                >
                  Editar
                </Link>
                <button
                  className="bg-red-600 text-white font-bold rounded-md px-2 py-1"
                  onClick={() => doDeleteEmployee(employee.id as number)}
                >
                  Eliminar
                </button>
              </td>
            </tr>
          ))}
      </Table>
    </>
  );
};

export default Employees;
