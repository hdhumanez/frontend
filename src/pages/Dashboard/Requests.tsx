import { useEffect, useState } from "react";
import useAuth from "../../context/useAuth";
import { REQUESTS_HEADER_TABLE } from "../../utils/constants/table";
import Table from "../../components/Table";
import { IRequest } from "../../interfaces/request.interface";
import { deleteRequest, getRequests } from "../../services/request.service";
import { Link } from "react-router-dom";

const Request = () => {
  const [requests, setRequests] = useState<Array<IRequest>>([]);
  const { auth } = useAuth();

  const doDeleteRequest = (id: number) => {
    deleteRequest(id, auth.token).then((response) => {
      if (response.success) {
        setRequests(requests.filter((request) => request.request_id !== id));
      }
    });
  };

  useEffect(() => {
    getRequests(auth.token).then((data) => setRequests(data.data));
  }, [auth.token]);

  return (
    <Table
      title="Lista de peticiones"
      description="Esta es la lista de peticiones de tus empleados"
      header={REQUESTS_HEADER_TABLE}
    >
      {requests.map((request, index) => (
        <tr className={`${index % 2 == 1 ? "bg-gray-50" : null}`}>
          <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-900">
            {request.request_code}
          </td>
          <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-500">
            {request.request_description}
          </td>
          <td className="p-4 whitespace-nowrap text-sm font-normal text-gray-900">
            {request.request_resume}
          </td>
          <td className="p-4 request_whitespace-nowrap text-sm font-normal text-gray-500">
            {request.employee_name}
          </td>
          <td className="p-4 whitespace-nowrap text-gray-600">
            <Link
              to={`/dashboard/requests/${request.request_id}/edit`}
              state={request}
              className="bg-yellow-600 mr-3 text-white font-bold rounded-md px-2 py-1"
            >
              Editar
            </Link>
            <button
              className="bg-red-600 text-white font-bold rounded-md px-2 py-1"
              onClick={() => doDeleteRequest(request.request_id)}
            >
              Eliminar
            </button>
          </td>
        </tr>
      ))}
    </Table>
  );
};

export default Request;
