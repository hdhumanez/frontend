import { addRequest } from "../../services/request.service";
import useAuth from "../../context/useAuth";
import toast, { Toaster } from "react-hot-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { TRequestSchema, requestSchema } from "../../schemas/request.schema";
import { useForm } from "react-hook-form";
import { IEmployee } from "../../interfaces/employee.interface";
import { useEffect, useState } from "react";
import { getEmployees } from "../../services/employee.service";

const AddRequest = () => {
  const { auth } = useAuth();
  const [employees, setEmployees] = useState<IEmployee[]>([]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TRequestSchema>({
    resolver: zodResolver(requestSchema),
  });

  const onSubmit = (data: TRequestSchema) => {
    addRequest(data, auth.token).then((response) => {
      if (response.success) {
        toast.success("Solicitud guardada correctamente");
      } else {
        return toast.error(response.info);
      }
    });
  };

  useEffect(() => {
    getEmployees(auth.token).then((response) => {
      if (response.success) {
        console.log(response.data);
        setEmployees(response.data);
      }
    });
  }, [auth.token]);

  return (
    <section className="flex items-center justify-center md:p-24">
      <Toaster />
      <form
        className="flex flex-col items-center gap-2"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="description">
            Descripción
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("description")}
            placeholder="Falto el pago de nómina Abril"
          />
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="resume">
            Resumen
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("resume")}
            placeholder="Pago nómina Abril"
          />
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-300" htmlFor="employee_id">
            Empleado
          </label>

          <select
            className="w-full rounded-lg border border-gray-300 py-2 px-3 text-sm"
            {...register("employee_id", {
              value: employees[0]?.id,
              valueAsNumber: true,
            })}
          >
            {employees.length > 0 &&
              employees.map((emp) => (
                <option value={emp.id} key={emp.id}>
                  {emp.name}
                </option>
              ))}
          </select>
          <p className="text-xs text-red-200">{errors.employee_id?.message}</p>
        </div>
        <button
          type="submit"
          className="w-full sm:w-1/2 mt-6 rounded-lg text-white bg-cyan-600 hover:bg-cyan-700 py-2 font-medium shadow-lg transition-colors"
        >
          Guardar solicitud
        </button>
      </form>
    </section>
  );
};

export default AddRequest;
