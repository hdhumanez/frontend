import useAuth from "../../context/useAuth";
import toast, { Toaster } from "react-hot-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { useEffect } from "react";
import { updateEmployee } from "../../services/employee.service";
import { useLocation, useParams } from "react-router-dom";
import { employeeSchema, TEmployeeSchema } from "../../schemas/employee.schema";

const EditEmployee: React.FC = () => {
  const { auth } = useAuth();
  const { state } = useLocation();
  const { id } = useParams<{ id: string }>();
  const { name, salary, admission_date } = state as TEmployeeSchema;

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<TEmployeeSchema>({
    resolver: zodResolver(employeeSchema),
    defaultValues: {
      name,
      admission_date,
      salary,
    },
  });

  useEffect(() => {
    reset({ name, salary });
  }, [name, salary, reset]);

  const onSubmit = (data: TEmployeeSchema) => {
    console.log(data);
    if (!id) return toast.error("No se ha encontrado el id de la solicitud");
    updateEmployee(id, data, auth.token).then((response) => {
      if (response.success) {
        toast.success("Solicitud actualizada correctamente");
      } else {
        return toast.error(response.info);
      }
    });
  };

  return (
    <section className="flex items-center justify-center md:p-24">
      <Toaster />
      <form
        className="flex flex-col items-center gap-2"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="name">
            Nombre completo
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("name")}
            defaultValue={name}
            placeholder="Falto el pago de nómina Abril"
          />
          <p className="text-xs text-red-200">{errors.name?.message}</p>
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="admission_date">
            Fecha de admisión
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            type="date"
            {...register("admission_date")}
          />
          <p className="text-xs text-red-200">
            {errors.admission_date?.message}
          </p>
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="salary">
            Salario
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("salary", { valueAsNumber: true })}
          />
          <p className="text-xs text-red-200">{errors.salary?.message}</p>
        </div>
        <button
          type="submit"
          className="w-full sm:w-1/2 mt-6 rounded-lg text-white bg-cyan-600 hover:bg-cyan-700 py-2 font-medium shadow-lg transition-colors"
        >
          Actualizar empleado
        </button>
      </form>
    </section>
  );
};

export default EditEmployee;
