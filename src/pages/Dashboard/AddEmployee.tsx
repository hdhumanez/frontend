import { addEmployee } from "../../services/employee.service";
import useAuth from "../../context/useAuth";
import toast, { Toaster } from "react-hot-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { TEmployeeSchema, employeeSchema } from "../../schemas/employee.schema";
import { useForm } from "react-hook-form";

const AddEmployee = () => {
  const { auth } = useAuth();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TEmployeeSchema>({
    resolver: zodResolver(employeeSchema),
  });

  const onSubmit = (data: TEmployeeSchema) => {
    addEmployee(data, auth.token).then((response) => {
      if (response.success) {
        toast.success("Empleado guardado correctamente");
      } else {
        return toast.error(response.info);
      }
    });
  };

  return (
    <section className="flex items-center justify-center md:p-24">
      <Toaster />
      <form
        className="flex flex-col items-center gap-2"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="name">
            Nombre
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("name")}
            placeholder="Joris Voorn"
          />
        </div>
        <div className="w-full mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="admission_date">
            Fecha de admisión
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3 text-sm"
            type="date"
            {...register("admission_date")}
          />
          <p className="text-xs text-red-200">
            {errors.admission_date?.message}
          </p>
        </div>
        <div className="w-96 mb-1">
          <label className="block mb-1 text-gray-100" htmlFor="">
            Salario
          </label>
          <input
            className="w-full rounded-lg border border-gray-300 py-2 px-3"
            {...register("salary", { valueAsNumber: true })}
            placeholder="4500000"
          />
        </div>
        <button
          type="submit"
          className="w-full sm:w-1/2 mt-6 rounded-lg text-white bg-cyan-600 hover:bg-cyan-700 py-2 font-medium shadow-lg transition-colors"
        >
          Guardar empleado
        </button>
      </form>
    </section>
  );
};

export default AddEmployee;
