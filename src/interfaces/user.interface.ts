export interface IUser {
  email: string;
  name: string;
}

export interface IUserContext {
  auth: {
    admin: {
      id: number;
      name: string;
      email: string;
    };
    token: string;
  };
  setAuth: (auth: IUserContext["auth"]) => void;
}
