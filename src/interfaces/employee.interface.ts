export interface IEmployee {
  id: number;
  admission_date: string;
  name: string;
  salary: number;
}
