export interface IRequest {
  request_id: number;
  request_code: string;
  request_description: string;
  request_resume: string;
  employee_name: string;
  employee_id: number;
}
